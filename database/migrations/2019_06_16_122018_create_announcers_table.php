<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',300)->nullable();
            $table->string('profile_image',500);
            $table->string('nickname');
            $table->date  ('birthday');
            $table->string('Brief');
            $table->string('place_birth');
            $table->string('tower');
            $table->string('graduate',200);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('announcers');
    }
}
