<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChangeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('days_programs', function (Blueprint $table) {
           /* $table->bigIncrements('id');*/
            $table->unsignedBigInteger('id_program')->change();
            $table->unsignedBigInteger('id_day')->change();
            $table->foreign('id_program')->references('id')->on('programs')->onDelete('cascade');
            $table->foreign('id_day')->references('id')->on('days')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('change');
    }
}
