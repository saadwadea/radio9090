<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('announcers_programs',function (Blueprint $table){
            $table->unsignedBigInteger('id_program')->change();
            $table->unsignedBigInteger('id_announcer')->change();
           $table->foreign('id_program')->references('id')->on('programs')->onDelete('cascade');
           $table->foreign('id_announcer')->references('id')->on('announcers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
