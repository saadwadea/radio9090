<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('days')->insert(array(
            array(
                'name' => 'السبت',
                'name_en' => 'Saturday'
            ),
            array(
                'name' => 'الأحد',
                'name_en' => 'Sunday'
            ),
            array(
                'name' => 'الاثنين',
                'name_en' => 'Monday'
            ),
            array(
                'name' => 'الثلاثاء',
                'name_en' => 'Tuesday'
            ),
            array(
                'name' => 'الأربعاء',
                'name_en' => 'wednesday'
            ),
            array(
                'name' => 'الخميس',
                'name_en' => 'Thursday'
            ),
            array(
                'name' => 'الجمعة',
                'name_en' => 'Friday'
            ),
        ));
    }
}
