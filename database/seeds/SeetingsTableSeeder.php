<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SeetingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert(array(
            array(
                'key_ar' => 'رقم التليفون',
                'key_en' => 'phone number',
            ),
            array(
                'key_ar' => 'رقم الرسالة',
                'key_en' => 'message',
            ),
            array(
                'key_ar' => 'فيسبوك ',
                'key_en' => 'facebook ',
            ),
            array(
                'key_ar' => 'انستجرام',
                'key_en' => 'instagram',
            ),
            array(
                'key_ar' => 'يوتيوب',
                'key_en' => 'youtube',
            ),
            array(
                'key_ar' => 'الموقع',
                'key_en' => 'website',
            ),
            array(
                'key_ar' => 'عن الأسم',
                'key_en' => 'About name',
            ),
            array(
                'key_ar' => 'عن البرنامج',
                'key_en' => 'About program',
            ),
            array(
                'key_ar' => 'عن راديو 9090',
                'key_en' => 'About',
            ),
        ));
    }
}
