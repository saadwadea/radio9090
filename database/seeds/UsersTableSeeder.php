<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new User([
            'name'=>'admin',
            'email'=>'admin@admin.com',
            'password'=> Hash::make('123456'),
            'remember_token' => str_random(10),
            'email_verified_at'=> Carbon::now()
        ]))->save();
    }
}
