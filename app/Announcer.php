<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Announcer extends Model
{
    use HasTranslations;

    public $translatable = ['name','nickname','Brief','place_birth','tower','graduate'
    ];
    protected $fillable=[
        'name','profile_image','nickname','birthday','Brief','place_birth','tower','graduate'
    ];

    public function program()
    {
        return $this->belongsToMany(Program::class,'announcers_programs','id_announcer','id_program');
    }
}
