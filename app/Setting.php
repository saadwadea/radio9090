<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable=[

        'key_ar','value_ar',
        'key_en','value_en'
    ];
}
