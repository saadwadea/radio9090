<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Program extends Model
{
    use HasTranslations;
    protected $fillable=[
        'name','img_url_blueprint','description','full_time_program','time_program'
    ];
   public $translatable= ['name','description','full_time_program'];
    public function day()
    {
        return $this->belongsToMany(Day::class,'days_programs','id_program','id_day');
    }
    public function announcer()
    {
        return $this->belongsToMany(Announcer::class,'announcers_programs','id_announcer','id_program');
    }
}
