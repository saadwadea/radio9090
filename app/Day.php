<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Day extends Model
{
    public function program()
    {
        return $this->belongsToMany(Program::class,'days_programs','id_day','id_program');
    }
}
