<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('_title', function($key) {

            return "{{ __('title.'.${key}) }}";
        });

       /* Blade::directive('_label', function($key) {
            return  "{{ __('labels.'.${key}) }}";
        });*/

    }
}
