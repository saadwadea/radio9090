<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class Days extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $timeFunction = function ($time) {
            $carbon = new Carbon($time->time_program);
            $time->time_program = str_replace(' AM', ''.__('title.time_am'), str_replace(' PM', ''.__('title.time_am'), $carbon->format('h:i A')));
            return $time;
        };
        //$day->program()->map($timeFunction);
        return [
            'day' => $this->name,
            'program'=>  Program::collection($this->program->map($timeFunction)),


        ];
    }
}
