<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AOP extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'announcer' => $this->announcer,
            'programs' => $this->program,
        ];
    }
}
