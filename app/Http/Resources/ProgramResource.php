<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProgramResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'img_url_blueprint'=>url('images/'.$this->img_url_blueprint),
            'description'=>$this->description,
            'full_time_program'=>$this->full_time_program,
            'time_program'=>$this->time_program,
            'id_announcer'=>  $this->announcer->pluck('id')->toArray(),
            'announcer'=>$this->announcer,
        ];
    }
}
