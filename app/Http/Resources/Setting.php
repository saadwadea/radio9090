<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Setting extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'key_ar'=>$this->key_ar,
            'key_en'=>$this->key_en,
            'value_ar'=>$this->value_ar,
            'value_en'=>$this->value_en,
        ];
    }
}
