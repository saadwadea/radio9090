<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class AnnouncerOfProgram extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $timeFunction = function ($time) {
            $carbon = new Carbon($time->time_program);
            $time->time_program = str_replace(' AM', '' . __('title.time_am'), str_replace(' PM', '' . __('title.time_am'), $carbon->format('h:i A')));
            return $time;
        };
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'profile_image'=>url('images/'.$this->profile_image),
            'nickname'=>$this->nickname,
            'birthday'=>$this->birthday,
            'Brief'=>$this->Brief,
            'place_birth'=>$this->place_birth,
            'tower'=>$this->tower,
            'graduate'=>$this->graduate,
            'name_program'=>  $this->program->first()->name,
            'id_program'=>  $this->program->pluck('id')->toArray(),
            'program'=>  Program::collection($this->program->map($timeFunction)),
            ];
    }
}
