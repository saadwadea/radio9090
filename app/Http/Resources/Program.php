<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class Program extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'img_url_blueprint'=>url('images/'.$this->img_url_blueprint),
            'description'=>$this->description,
            'full_time_program'=>$this->full_time_program,
            'time_program'=>$this->time_program,
            'name_announcer'=>$this->announcer->first()->name,
            'image_announcer'=>url('images/'.$this->announcer->first()->profile_image),
        ];
    }
}
