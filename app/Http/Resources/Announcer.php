<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Announcer extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'profile_image'=>url('images/'.$this->profile_image),
            'nickname'=>$this->nickname,
            'birthday'=>$this->birthday,
            'Brief'=>$this->Brief,
            'place_birth'=>$this->place_birth,
            'tower'=>$this->tower,
            'graduate'=>$this->graduate,
            'name_program'=> $this->program->first()->name,
        ];
    }
}
