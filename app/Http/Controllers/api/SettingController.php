<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;

use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index()
    {
        $setting = Setting::all();

        return \App\Http\Resources\Setting::collection($setting);

    }
}
