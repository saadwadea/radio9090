<?php

namespace App\Http\Controllers\api;
use App\Announcer;
use App\Day;
use App\Http\Controllers\Controller;

use App\Http\Resources\Days;
use App\Http\Resources\ProgramResource;
use App\Program;
use Carbon\Carbon;
use Illuminate\Http\Request;


class ProgramController extends Controller
{
    public function index(){
        $timeFunction = function ($time){
            $carbon = new Carbon($time->time_program);
            $time->time_program=str_replace(' AM',''.__('title.time_am'),str_replace(' PM',''.__('title.time_pm'),$carbon->format('h:i A' )));
            return $time;
        };
        $program=Program::all()->map($timeFunction);
        return \App\Http\Resources\Program::collection($program);
    }
    public function programByDay(Request $request){

        $day = Day::where('name',$request->day_name)->first();

        if(!$day) abort(404);

        return new Days($day);
    }
    public function programOfAnnouncer(){

        $program = Program::all()->load('announcer');

        return ProgramResource::collection($program);
    }
}
