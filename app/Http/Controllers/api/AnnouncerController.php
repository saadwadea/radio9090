<?php

namespace App\Http\Controllers\api;

use App\Announcer;
use App\Http\Controllers\Controller;
use App\Http\Resources\AnnouncerOfProgram;
use App\Http\Resources\ProgramResource;
use App\Program;
use Illuminate\Http\Request;

class AnnouncerController extends Controller
{
    public function index(){

        $announcer=Announcer::all();
        return \App\Http\Resources\Announcer::collection($announcer);

    }
    public function announcerOfProgram(){


        $announcer = Announcer::all()->load('program');

        return AnnouncerOfProgram::collection($announcer);
    }

}
