<?php

namespace App\Http\Controllers;

use App\Http\Requests\messages_programs;
use App\Program;
use Carbon\Carbon;
use Illuminate\Http\Request;

class programController extends Controller
{
    public function index()
    {
        $timeFunction = function ($time){
            $carbon = new Carbon($time);
            return str_replace('AM',''.__('title.time_am'),str_replace('PM',''.__('title.time_pm'),$carbon->format('h:i A' )));
        };
        return view('program.index',compact('timeFunction'));
    }
    public function create()
    {
        return view('program.create');
    }

    public function store(messages_programs $request)
    {
        //
        $program=new Program($request->all());
        if(request()->img_url_blueprint){
            $imageName = time().'.'.request()->img_url_blueprint->getClientOriginalExtension();
            request()->img_url_blueprint->move(public_path('images'), $imageName);
            $program->img_url_blueprint = $imageName;
        }
        $program->save();
        $program->day()->attach($request->day);
        return redirect('program');
    }
    public function show($id)
    {
        $program=Program::find($id);
        return view('program.show')->with(["program"=>$program]);
    }
    public function edit($id)
    {
        //
        $program=Program::find($id);

        return view('program.edit')->with(["program"=>$program]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $program=Program::find($id);
        $program->day()->sync($request->day);
        $program->update(array_filter($request->all(),function ($key){
            return $key != null;
        }));

        if (request()->img_url_blueprint){
            $imageName = time().'.'.request()->img_url_blueprint->getClientOriginalExtension();
            request()->img_url_blueprint->move(public_path('images'), $imageName);
            $program->img_url_blueprint = $imageName;
        }

        $program->save();
        return redirect('program');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $program=Program::find($id);
        $program->delete();
        return redirect('program');
    }
}
