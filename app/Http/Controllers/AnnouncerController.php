<?php

namespace App\Http\Controllers;

use App\Announcer;
use App\Http\Requests\messages;
use Illuminate\Http\Request;

class AnnouncerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('announcer.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('announcer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(messages $request)
    {
        $announcer=new Announcer($request->all());
        if(request()->profile_image){
            $imageName = time().'.'.request()->profile_image->getClientOriginalExtension();
            request()->profile_image->move(public_path('images'), $imageName);
            $announcer->profile_image = $imageName;
        }

        $announcer->save();
        $announcer->program()->attach($request->programs);

        return redirect('announcer');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $announcer=Announcer::find($id);
        return view('announcer.show')->with(["announcer"=>$announcer]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $announcer=Announcer::find($id);

        return view('announcer.edit')->with(["announcer"=>$announcer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $announcer=Announcer::find($id);
        $announcer->program()->sync($request->programs);
        $announcer->update(array_filter($request->all(),function ($key){
            return $key != null;
        }));

        if (request()->profile_image){

            $imageName = time().'.'.request()->profile_image->getClientOriginalExtension();
            request()->profile_image->move(public_path('images'), $imageName);
            $announcer->profile_image = $imageName;
        }

        $announcer->save();
        return redirect('announcer');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $announcer=Announcer::find($id);
        $announcer->delete();
        return redirect('announcer');
    }
}
