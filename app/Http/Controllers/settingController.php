<?php

namespace App\Http\Controllers;

use App\Setting;
use App\Settings;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class settingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $setting = Setting::all();

        return view('setting')->with(["settings" => $setting]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if ($request->settings) {
            if (LaravelLocalization::getCurrentLocale()  == 'ar')
            foreach ($request->settings as $key => $value) {
                $settings = Setting::where('key_ar', $key)->firstOrFail();
                $settings->value_ar = $value;
                $settings->save();
            }
            elseif (LaravelLocalization::getCurrentLocale()  == 'en'){
                foreach ($request->settings as $key => $value) {
                    $settings = Setting::where('key_en', $key)->firstOrFail();
                    $settings->value_en = $value;
                    $settings->save();
                }
            }
        }
        return redirect('setting');
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
