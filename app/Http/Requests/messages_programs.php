<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class messages_programs extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'img_url_blueprint' => 'required',
            'description' => 'required',
            'full_time_program' => 'required',
            'time_program' => 'required',
        ];
    }
    public function messages()
    {
        return [

        ];
    }
}
