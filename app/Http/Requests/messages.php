<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class messages extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'profile_image' => 'required',
            'nickname' => 'required',
            'birthday' => 'required',
            'Brief' => 'required',
            'place_birth' => 'required',
            'tower' => 'required',
            'graduate' => 'required',
            'programs' => 'required',
        ];
    }
    public function messages()
    {
        return [

        ];
    }
}
