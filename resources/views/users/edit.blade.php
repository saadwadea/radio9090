@extends('layout.dashboard')
@section('content')

    <!--  page-wrapper -->
    <div id="page-wrapper">

        <div class="row">
            <!-- Page Header -->
            <div class="col-lg-12">
                <h1 class="page-header">edit profile</h1>
            </div>
            <!--End Page Header -->
        </div>

        <form method="post" action="{{ route('users.update', $user->id)}}">
            {{ csrf_field() }}
            {{ method_field('patch') }}

            <input type="text" name="name"  value="{{ $user->name }}" />

            <input type="email" name="email"  value="{{ $user->email }}" />

            <input type="password" name="password" />

            <input type="password" name="password_confirmation" />

            <button type="submit">Send</button>
        </form>

    </div>
    <!-- end page-wrapper -->
@endsection
