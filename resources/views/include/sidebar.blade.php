<nav class="navbar-default navbar-static-side" role="navigation">
    <!-- sidebar-collapse -->
    <div class="sidebar-collapse">
        <!-- side-menu -->
        <ul class="nav" id="side-menu">
            <li>
                <!-- user image section-->
                <div class="user-section">
                    <div class="user-section-inner">
                        <img src="{{url('assets/img/user.jpg')}}" alt="">
                    </div>
                    <div class="user-info">
                        <div> <strong>{{Auth::user()->name }}</strong></div>
                        <div class="user-text-online">
                            <span class="user-circle-online btn btn-success btn-circle "></span>&nbsp;@_title('online')
                        </div>
                    </div>
                </div>
                <!--end user image section-->
            </li>
          {{--  <li class="sidebar-search">
                <!-- search section-->
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                </div>
                <!--end search section-->
            </li>--}}
            <li class="">
                <a href="{{url('/')}}"><i class="fa fa-dashboard fa-fw"></i>@_title('dashboard')</a>
            </li>
         {{--   <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i>Charts<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{url('/flot')}}">Flot Charts</a>
                    </li>
                    <li>
                        <a href="{{url('/morris')}}">Morris Charts</a>
                    </li>
                </ul>
                <!-- second-level-items -->
            </li>--}}
            <li>
                <a href="#"><i class="fa fa-clipboard"></i></n>@_title('announcers')<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{URL::route('announcer.index')}}">@_title('show_all')</a>
                    </li>
                    <li>
                        <a href="{{URL::route('announcer.create')}}">@_title('add')</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-desktop"></i>@_title('programs')<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{URL::route('program.index')}}">@_title('show_all')</a>

                    </li>
                    <li>
                        <a href="{{URL::route('program.create')}}">@_title('add')</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{url('setting')}}" class="fa fa-edit">@_title('settings')</a>

            </li>
           {{-- <li class="active">
                <a href="#"><i class="fa fa-wrench fa-fw"></i>UI Elements<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{url('/panelwell')}}">Panels and Wells</a>
                    </li>
                    <li>
                        <a href="{{url('/button')}}">Buttons</a>
                    </li>
                    <li>
                        <a href="{{url('/notifications')}}">Notifications</a>
                    </li>
                    <li>
                        <a href="{{url('/typography')}}">Typography</a>
                    </li>
                </ul>
                <!-- second-level-items -->
            </li>--}}
          {{--  <li>
                <a href="#"><i class="fa fa-sitemap fa-fw"></i>Multi-Level Dropdown<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="#">Second Level Item</a>
                    </li>
                    <li>
                        <a href="#">Second Level Item</a>
                    </li>
                    <li>
                        <a href="#">Third Level <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="#">Third Level Item</a>
                            </li>
                            <li>
                                <a href="#">Third Level Item</a>
                            </li>
                            <li>
                                <a href="#">Third Level Item</a>
                            </li>
                            <li>
                                <a href="#">Third Level Item</a>
                            </li>
                        </ul>
                        <!-- third-level-items -->
                    </li>
                </ul>
                <!-- second-level-items -->
            </li>--}}
           {{-- <li class="">
                <a href="#"><i class="fa fa-files-o fa-fw"></i>Sample Pages<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li class="">
                        <a href="{{url('/blank')}}">Blank Page</a>
                    </li>
                    <li>
                        <a href="{{url('/login')}}">Login Page</a>
                    </li>
                </ul>
                <!-- second-level-items -->
            </li>--}}
        </ul>
        <!-- end side-menu -->
    </div>
    <!-- end sidebar-collapse -->
</nav>
<!-- end navbar side -->