<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@_title('radio')</title>
    <!-- Core CSS - Include with every page -->
    <link href="{{url('assets/plugins/bootstrap/bootstrap.css')}}" rel="stylesheet" />
    <link href="{{url('assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
    <link href="{{url('assets/plugins/pace/pace-theme-big-counter.css')}}" rel="stylesheet" />
    <link href="{{url('assets/css/style.css')}}" rel="stylesheet" />
    <link href="{{url('assets/css/main-style.css')}}" rel="stylesheet" />
    <link href="{{url('assets/plugins/social-buttons/social-buttons.css')}}" rel="stylesheet" />
    <link href="{{url('assets/plugins/morris/morris-0.4.3.min.css')}}" rel="stylesheet" />
    <link href="{{url('assets/plugins/dataTables/dataTables.bootstrap.css')}}" rel="stylesheet" />
    <link href="{{url('assets/plugins/timeline/timeline.css')}}" rel="stylesheet" />
    <style>
        .photoPreview{
            width: 100px;
            height: 100px;
        }
        .picTable{
            width: 40px;
            height: 40px;
        }
    </style>

    @yield('style')
</head>

<body>

<div id="wrapper">

    @include('include.header')
    @include('include.sidebar')
    @yield('content')
</div>


<script src="{{url('assets/plugins/jquery-1.10.2.js')}}"></script>
<script src="{{url('assets/plugins/bootstrap/bootstrap.min.js')}}"></script>
<script src="{{url('assets/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
<script src="{{url('assets/plugins/pace/pace.js')}}"></script>
<script src="{{url('assets/scripts/siminta.js')}}"></script>
<script src="{{url('assets/plugins/flot/jquery.flot.js')}}"></script>
<script src="{{url('assets/plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{url('assets/plugins/flot/jquery.flot.resize.js')}}"></script>
<script src="{{url('assets/plugins/flot/jquery.flot.pie.js')}}"></script>
<script src="{{url('assets/scripts/flot-demo.js')}}"></script>
<script src="{{url('assets/plugins/morris/raphael-2.1.0.min.js')}}"></script>
<script src="{{url('assets/plugins/morris/morris.js')}}"></script>
<script src="{{url('assets/scripts/dashboard-demo.js')}}"></script>
<script src="{{url('assets/plugins/morris/raphael-2.1.0.min.js')}}"></script>
<script src="{{url('assets/plugins/morris/morris.js')}}"></script>
<script src="{{url('assets/scripts/morris-demo.js')}}"></script>
<script src="{{url('assets/plugins/dataTables/jquery.dataTables.js')}}"></script>
<script src="{{url('assets/plugins/dataTables/dataTables.bootstrap.js')}}"></script>


<script>
    $(document).ready(function () {
        $('#dataTables-example').dataTable();
    });
    $(".logoutBtn").click(function (e) {
        e.preventDefault();
        $("#logout-form").submit();
    });
    $("#color option").val(function(idx, val) {
        $(this).siblings("[value='"+ val +"']").remove();
    });
</script>
@yield('script')
</body>
</html>