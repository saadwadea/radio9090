@extends('layout.dashboard')
@section('content')
    <!--main content start-->
    <div id="page-wrapper">
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><i class="fa fa-file-text-o"></i>@_title('add_program')</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <div class="panel-body">
                            <form class="form-horizontal " method="post" enctype="multipart/form-data" action="{{route('program.store')}}">
                                @csrf

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">@_title('name_program')</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name[ar]" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">@_title('name_program_lan')</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name[en]" required>
                                    </div>
                                </div>


                            <div class="form-group">
                                <label class="col-sm-2 control-label">@_title('image_blueprint')</label>
                                <div class="col-sm-10">
                                    <input type="file" class="form-control" name="img_url_blueprint" required>
                                </div>
                            </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">@_title('description')</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="description[ar]" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">@_title('description_lan')</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="description[en]" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">@_title('fulltime_program')</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="full_time_program[ar]" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">@_title('fulltime_program_lan')</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="full_time_program[en]" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">@_title('time_program')</label>
                                    <div class="col-sm-10">
                                        <input type="time" class="form-control" name="time_program" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">@_title('day')</label>
                                    <div class="col-sm-10">
                                        <select class="form-control"  name="day" required>
                                            @php($var = \Illuminate\Support\Facades\App::getLocale() == 'ar' ? 'name' : 'name_en')
                                            @foreach(\App\Day::all() as $day)
                                                <option value="{{$day->id}}">{{$day->$var}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                <div class="row text-center">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button class="btn btn-default center-block btn-success" type="submit">@_title('add')</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                            </form>
                        </div>

                    </section>

                </div>
            </div>
            <!-- Basic Forms & Horizontal Forms-->


        </section>
    </section>
    <!--main content end-->
    </div>

@endsection
