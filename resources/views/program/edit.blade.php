@extends('layout.dashboard')
@section('content')
    <div id="page-wrapper">
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa-file-text-o"></i>@_title('edit')</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <div class="panel-body">
                        <form class="form-horizontal " method="post" enctype="multipart/form-data" action="{{route('program.update',['program'=>$program->id])}}">
                            @csrf
                            @method('put')

                            <div class="form-group">
                                <label class="col-sm-2 control-label">@_title('name_program')</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="name" value="{{$program->name}}">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-2 control-label">@_title('image_blueprint')</label>
                                <div class="col-sm-10">
                                    <img class="photoPreview" src="{{url('images/'.$program->img_url_blueprint)}}">
                                    <input type="file" class="form-control" name="img_url_blueprint" value="{{$program->img_url_blueprint}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">@_title('description')</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="description" value="{{$program->description}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">@_title('fulltime_program')</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="full_time_program" value="{{$program->full_time_program}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">@_title('time_program')</label>
                                <div class="col-sm-10">
                                    <input type="time" class="form-control" name="time_program" value="{{$program->time_program}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">@_title('day')</label>
                                <div class="col-sm-10">
                                    <select class="form-control"  name="day">
                                        @foreach(\App\Day::all() as $day)
                                            <option
                                                value="{{$day->id}}"
                                                @if($day->id == $program->day[0]->id)
                                                    selected
                                                @endif
                                                >{{$day->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row text-center">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button class="btn btn-default center-block btn-primary" type="submit">@_title('edit')</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </section>

            </div>
        </div>
        <!-- Basic Forms & Horizontal Forms-->

    </section>
</section>
    </div>
@endsection

