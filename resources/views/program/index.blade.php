@extends('layout.dashboard')

@section('content')
    <br>
    <br>
    <div id="page-wrapper">
    <section id="main-content">
        <section class="wrapper">
        <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="table table-hover table-responsive">
                </header>
                <table class="table table-bordred table-striped">
                    <thead>

                        <tr>
                        <th>@_title('name_program')</th>
                        <th>@_title('image_blueprint')</th>
                        <th>@_title('description')</th>
                        <th>@_title('fulltime_program')</th>
                        <th>@_title('time_program')</th>
                        <th>@_title('day')</th>
                    </tr>

                    </thead>
                    <tbody>
                    @foreach(\App\Program::all() as $program)

                    <tr>
                        <td>{{$program->name}}</td>
                        <td><img class="picTable" src="{{url('images/'.$program->img_url_blueprint)}}"></td>
                        <td>{{$program->description}}</td>
                        <td>{{$program->full_time_program}}</td>
                        <td>{{$timeFunction($program->time_program)}}</td>
                        <td>
                            @if (LaravelLocalization::getCurrentLocale()  == 'ar')
                            @foreach($program->day()->pluck('name') as $name)
                                {{$name }}
                                <br>
                            @endforeach
                            @elseif(LaravelLocalization::getCurrentLocale()  == 'en')
                                @foreach($program->day()->pluck('name_en') as $name)
                                    {{$name }}
                                    <br>
                                @endforeach
                                @endif
                        </td>
                        <td><a class="btn btn-small btn-primary" href="{{route('program.edit',['$program'=>$program->id])}}">@_title('edit')</a></td>
                        <td><form class="form-horizontal " method="post" action="{{route('program.destroy',['$program'=>$program->id])}}"> @method('DELETE') @csrf <input type="submit" value="@_title('delete')" class="btn btn-small btn-danger"> </form></td>
                    </tr>
                    @endforeach
                </table>
            </section>
        </div>
    </div>
        </section>
    </section>
    </div>
@endsection