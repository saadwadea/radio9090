@extends('layout.dashboard')
@section('content')

    <div id="page-wrapper">
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa-file-text-o"></i>@_title('settings')</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <div class="panel-body">
                        <form class="form-horizontal " method="post" action="{{route('setting')}}">
                            @csrf
                            @method('put')
                            @if (LaravelLocalization::getCurrentLocale()  == 'ar')
                            @foreach($settings as $setting)
                            <div class="form-group">
                                <label class="col-sm-2 control-label">{{$setting->key_ar}}</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="settings[{{$setting->key_ar}}]" value="{{$setting->value_ar}}">
                                </div>
                            </div>
                            @endforeach

                            @elseif(LaravelLocalization::getCurrentLocale()  == 'en')
                                @foreach($settings as $setting)
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">{{$setting->key_en}}</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="settings[{{$setting->key_en}}]" value="{{$setting->value_en}}">
                                        </div>
                                    </div>
                                @endforeach
                            @endif

                            <div class="row text-center">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button class="btn btn-default center-block btn-primary" type="submit">@_title('edit')</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </section>

            </div>
        </div>
        <!-- Basic Forms & Horizontal Forms-->


    </section>
</section>
    </div>
@endsection
