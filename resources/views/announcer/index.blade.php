@extends('layout.dashboard')

@section('content')
    <br>
    <br>
    <div id="page-wrapper">
        <section id="main-content">
            <section class="wrapper">
                <div class="row">
                    <div class="col-sm-12">
                        <section class="panel">
                            <header class="table table-hover table-responsive">
                            </header>
                            <table class="table table-bordred table-striped">
                    <thead>

                        <tr>
                        <th>@_title('name')</th>
                        <th>@_title('image_announcer')</th>
                        <th>@_title('nickname')</th>
                        <th>@_title('date_birth')</th>
                        <th>@_title('brief')</th>
                        <th>@_title('day_birth')</th>
                        <th>@_title('tower')</th>
                        <th>@_title('graduate')</th>
                        <th>@_title('programs')</th>
                    </tr>

                    </thead>
                    <tbody>
                    @foreach(\App\Announcer::all() as $announcer)

                    <tr>
                        <td> {{$announcer->name}}</td>
                        <td><img class="picTable" src="{{url('images/'.$announcer->profile_image)}}" ></td>
                        <td>{{$announcer->nickname}}</td>
                        <td>{{$announcer->birthday}}</td>
                        <td>{{$announcer->Brief}}</td>
                        <td>{{$announcer->place_birth}}</td>
                        <td>{{$announcer->tower}}</td>
                        <td>{{$announcer->graduate}}</td>
                        <td>
                            @foreach($announcer->program()->pluck('name')->toArray() as $name)
                                {{$name }}
                                <br>
                                @endforeach
                        </td>
                        <td><a class="btn btn-small btn-primary" href="{{route('announcer.edit',['$announcer'=>$announcer->id])}}">@_title('edit')</a></td>
                        <td><form class="form-horizontal " method="post" action="{{route('announcer.destroy',['$announcer'=>$announcer->id])}}">
                                @method('DELETE') @csrf <input type="submit" value="@_title('delete')" class="btn btn-small btn-danger"> </form></td>
                    </tr>
                    @endforeach
                </table>
                        </section>
                    </div>
                </div>
            </section>
        </section>
    </div>

@endsection