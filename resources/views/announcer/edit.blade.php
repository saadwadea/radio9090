@extends('layout.dashboard')

@section('content')
    <div id="page-wrapper">
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa-file-text-o"></i>@_title('edit')</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <div class="panel-body">
                        <form class="form-horizontal " enctype="multipart/form-data" method="post" action="{{route('announcer.update',['announcer'=>$announcer->id])}}">
                            @csrf
                            @method('put')
                            <div class="form-group">
                                <label class="col-sm-2 control-label">@_title('name')</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="name" value="{{$announcer->name}}">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-2 control-label">@_title('image_announcer')</label>
                                <div class="col-sm-10">
                                    <img class="photoPreview" src="{{url('images/'.$announcer->profile_image)}}">
                                    <input type="file" class="form-control" name="profile_image" value="{{$announcer->profile_image}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">@_title('nickname')</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="nickname" value="{{$announcer->nickname}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">@_title('date_birth')</label>
                                <div class="col-sm-10">
                                    <input type="date" class="form-control" name="birthday" value="{{$announcer->birthday}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">@_title('brief')</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="Brief" value="{{$announcer->Brief}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">@_title('day_birth')</label>
                                <div class="col-sm-10">
                                    <select  class="form-control" name="place_birth"  id="selectID">
                                        <optgroup label="@_title('day_birth')">
                                            <option selected="{{$announcer->place_birth}}">{{$announcer->place_birth}}</option>
                                            <option value="@_title('Cairo')">@_title('Cairo')</option>
                                            <option value="@_title('Alexandria')">@_title('Alexandria')</option>
                                            <option value="@_title('Ismailia')">@_title('Ismailia')</option>
                                            <option value="@_title('Aswan')">@_title('Aswan') </option>
                                            <option value="@_title('Assiut')">@_title('Assiut') </option>
                                            <option value="@_title('Luxor')">@_title('Luxor') </option>
                                            <option value="@_title('Red Sea')">@_title('Red Sea')</option>
                                            <option value="@_title('Behira')">@_title('Behira')</option>
                                            <option value="@_title('Beni Suef')">@_title('Beni Suef')</option>
                                            <option value="@_title('Port Said')">@_title('Port Said')</option>
                                            <option value="@_title('South Sinai')">@_title('South Sinai') </option>
                                            <option value="@_title('Giza') ">@_title('Giza') </option>
                                            <option value="@_title('Dakahlia')">@_title('Dakahlia')</option>
                                            <option value="@_title('Damietta') ">@_title('Damietta') </option>
                                            <option value="@_title('Sohag')">@_title('Sohag')</option>
                                            <option value="@_title('Suez')">@_title('Suez')</option>
                                            <option value="@_title('Sharkiah') ">@_title('Sharkiah')</option>
                                            <option value="@_title('North Sinai')">@_title('North Sinai') </option>
                                            <option value="@_title('Gharbya') ">@_title('Gharbya') </option>
                                            <option value="@_title('Fayoum') ">@_title('Fayoum') </option>
                                            <option value="@_title('Qalioubia')">@_title('Qalioubia')</option>
                                            <option value="@_title('Qena')">@_title('Qena') </option>
                                            <option value="@_title('Kafr El Sheikh')">@_title('Kafr El Sheikh')</option>
                                            <option value="@_title('Matrouh') ">@_title('Matrouh')</option>
                                            <option value="@_title('Monofia') ">@_title('Monofia') </option>
                                            <option value="@_title('Minya') ">@_title('Minya')</option>
                                            <option value="@_title('New Valley')">@_title('New Valley')</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">@_title('tower')</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="tower" id="selectID">
                                        <optgroup label="@_title('tower')">
                                            <option value="{{$announcer->tower}}">{{$announcer->tower}}</option>
                                            <option value="@_title('Capricorn')">@_title('Capricorn')</option>
                                            <option value="@_title('Aries')">@_title('Aries')</option>
                                            <option value="@_title('Aquarius')">@_title('Aquarius')</option>
                                            <option value="@_title('Taurus')">@_title('Taurus')</option>
                                            <option value="@_title('Gemini')">@_title('Gemini')</option>
                                            <option value="@_title('Cancer')">@_title('Cancer')</option>
                                            <option value="@_title('Leo')">@_title('Leo')</option>
                                            <option value="@_title('Virgo')">@_title('Virgo')</option>
                                            <option value="@_title('Libra')">@_title('Libra')</option>
                                            <option value="@_title('Scorpio')">@_title('Scorpio')</option>
                                            <option value="@_title('Sagittarius')">@_title('Sagittarius')</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">@_title('graduate')</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="graduate" value="{{$announcer->graduate}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">@_title('programs')</label>
                                <div class="col-sm-10">
                                    <select class="form-control" multiple name="programs[]">
                                        @foreach(\App\Program::all() as $program)
                                            <option
                                                    @if(in_array($program->id,$announcer->program->pluck('id')->toArray()))
                                                            selected
                                                    @endif
                                                    value="{{$program->id}}">{{$program->name}}
                                            </option>

                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row text-center">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button class="btn btn-default center-block btn-primary" type="submit">@_title('edit')</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </section>

            </div>
        </div>
        <!-- Basic Forms & Horizontal Forms-->


    </section>
</section>
    </div>
@endsection
@section('script')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>



    <script>
        $('select[name="programs[]"]').select2();

        $("#color option").val(function(idx, val) {
            $(this).siblings("[value='"+ val +"']").remove();
        });
    </script>
    <script>
        var optionValues =[];
        $('#selectID option').each(function(){
            if($.inArray(this.value, optionValues) >-1){
                $(this).remove()
            }else{
                optionValues.push(this.value);
            }
        });
    </script>
@endsection
