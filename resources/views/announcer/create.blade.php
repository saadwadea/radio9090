@extends('layout.dashboard')
@section('content')

    <div id="page-wrapper">
        <section id="main-content">
            <section class="wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header"><i class="fa fa-file-text-o"></i> @_title('add_announcer') </h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <div class="panel-body">
                                <form class="form-horizontal " method="post" enctype="multipart/form-data" action="{{route('announcer.store')}}">
                                    @csrf
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">@_title('name')</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="name[ar]" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">@_title('name_lan')</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="name[en]" required>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">@_title('image_announcer')</label>
                                        <div class="col-sm-10">

                                            <input type="file" class="form-control" name="profile_image" required>

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">@_title('nickname')</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="nickname[ar]" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">@_title('nickname_lan')</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="nickname[en]" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">@_title('date_birth')</label>
                                        <div class="col-sm-10">
                                            <input type="date" class="form-control" name="birthday" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">@_title('brief')</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="Brief[ar]" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">@_title('brief_lan')</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="Brief[en]" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">@_title('day_birth')</label>
                                        <div class="col-sm-10">
                                        <select  class="form-control" name="place_birth[ar]" required>
                                            <optgroup label="@_title('day_birth')">
                                                <option value="القاهرة">القاهرة</option>
                                                <option value="الإسكندرية">الإسكندرية</option>
                                                <option value="الإسماعيلية">الإسماعيلية</option>
                                                <option value="أسوان">أسوان</option>
                                                <option value="أسيوط">أسيوط</option>
                                                <option value="الأقصر">الأقصر</option>
                                                <option value="البحر الأحمر">البحر الأحمر</option>
                                                <option value="البحيرة">البحيرة</option>
                                                <option value="بني سويف">بني سويف</option>
                                                <option value="بورسعيد">بورسعيد</option>
                                                <option value="جنوب سيناء">جنوب سيناء</option>
                                                <option value="الجيزة">الجيزة</option>
                                                <option value="الدقهلية">الدقهلية</option>
                                                <option value="دمياط">دمياط</option>
                                                <option value="سوهاج">سوهاج</option>
                                                <option value="السويس">السويس</option>
                                                <option value="الشرقية">الشرقية</option>
                                                <option value="شمال سيناء">شمال سيناء</option>
                                                <option value="الغربية">الغربية</option>
                                                <option value="الفيوم">الفيوم</option>
                                                <option value="القليوبية">القليوبية</option>
                                                <option value="قنا">قنا</option>
                                                <option value="كفر الشيخ">كفر الشيخ</option>
                                                <option value="مطروح">مطروح</option>
                                                <option value="المنوفية">المنوفية</option>
                                                <option value="المنيا">المنيا</option>
                                                <option value="الوادي الجديد">الوادي الجديد</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">@_title('day_birth_lan')</label>
                                        <div class="col-sm-10">
                                            <select  class="form-control" name="place_birth[en]" required>
                                                <optgroup label="@_title('day_birth')">
                                                    <option value="Cairo">Cairo</option>
                                                    <option value="Alexandria">Alexandria</option>
                                                    <option value="Ismailia">Ismailia</option>
                                                    <option value="Aswan">Aswan </option>
                                                    <option value="Assiut">Assiut </option>
                                                    <option value="Luxor">Luxor </option>
                                                    <option value="Red Sea">Red Sea</option>
                                                    <option value="Behira">Behira</option>
                                                    <option value="Beni Suef">Beni Suef</option>
                                                    <option value=" Port Said"> Port Said</option>
                                                    <option value=" South Sinai "> South Sinai </option>
                                                    <option value="Giza ">Giza </option>
                                                    <option value="Dakahlia">Dakahlia</option>
                                                    <option value="Damietta ">Damietta </option>
                                                    <option value="Sohag">Sohag</option>
                                                    <option value="Suez">Suez</option>
                                                    <option value="Sharkiah ">Sharkiah</option>
                                                    <option value="North Sinai">North Sinai </option>
                                                    <option value="Gharbya ">Gharbya </option>
                                                    <option value="Fayoum ">Fayoum </option>
                                                    <option value="Qalioubia">Qalioubia</option>
                                                    <option value="Qena">Qena </option>
                                                    <option value="Kafr El Sheikh">Kafr El Sheikh</option>
                                                    <option value="Matrouh ">Matrouh</option>
                                                    <option value="Monofia ">Monofia </option>
                                                    <option value="Minya ">Minya</option>
                                                    <option value="New Valley">New Valley</option>
                                                </optgroup>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">@_title('tower')</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="tower[ar]" required>
                                                <optgroup label="@_title('tower')">
                                                    <option value="الجدي">الجدي</option>
                                                    <option value="الدلو">الدلو</option>
                                                    <option value="الحوت">الحوت</option>
                                                    <option value="الثور">الثور</option>
                                                    <option value="الجوزاء">الجوزاء</option>
                                                    <option value="السرطان">السرطان</option>
                                                    <option value="الأسد">الأسد</option>
                                                    <option value="العذراء">العذراء</option>
                                                    <option value="الميزان">الميزان</option>
                                                    <option value="العقرب">العقرب</option>
                                                    <option value="القوس">القوس</option>
                                                </optgroup>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">@_title('tower_lan')</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="tower[en]" required>
                                                <optgroup label="@_title('tower')">
                                                    <option value="Capricorn">Capricorn</option>
                                                    <option value="Aries ">Aries </option>
                                                    <option value="Aquarius ">Aquarius </option>
                                                    <option value="Taurus">Taurus</option>
                                                    <option value="Gemini">Gemini</option>
                                                    <option value="Cancer">Cancer</option>
                                                    <option value="Leo">Leo</option>
                                                    <option value="Virgo">Virgo</option>
                                                    <option value="Libra">Libra</option>
                                                    <option value="Scorpio">Scorpio</option>
                                                    <option value="Sagittarius ">Sagittarius </option>
                                                </optgroup>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">@_title('graduate')</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="graduate[ar]" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">@_title('graduate_lan')</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="graduate[en]" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">@_title('programs')</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" multiple name="programs[]" required >
                                               @foreach(\App\Program::all() as $program)
                                                <option value="{{$program->id}}">{{$program->name}}</option>
                                                   @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row text-center">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <button class="btn btn-default center-block btn-success" type="submit">@_title('add')</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        @if ($errors->any())
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                    </div>
                                </form>
                            </div>

                        </section>

                    </div>
                </div>
                <!-- Basic Forms & Horizontal Forms-->


            </section>
        </section>
    </div>
    <!-- end page-wrapper -->
@endsection

@section('script')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script>
        $('select[name="programs[]"]').select2();
    </script>
    @endsection