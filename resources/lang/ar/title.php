<?php

return [
    'dashboard' => 'لوحة القيادة',
    'logout' => 'تسجيل الخروج',
    'login' => 'تسجيل الدخول',
    'remember' => 'تذكرنى',
    'add_user' => 'اضافة مستخدم جديد',
    'name' => ' الأسم ',
    'name_lan' => ' الأسم باالانجليزى',
    'email' => 'البريد الالكترونى',
    'password' => 'كلمة السر',
    'password_confirmed' => 'تأكيد كلمة السر',
    'register' => 'التسجيل',
    'announcers' => 'المذيعيين',
    'programs' => 'البرامج',
    'settings' => 'التعديلات',
    'show_all' => 'عرض الكل',
    'add' => 'اضافة',
    'edit' => 'تعديل',
    'delete' => 'مسح',
    'online' => 'عبر الانترنت',
    'file' => 'لم تقم باختيار ملف',
    'radio' => 'راديو 9090',
    'time_am' => 'ص',
    'time_pm' => 'م',

    //setting content
    'phone' => 'رقم التليفون',
    'message' => 'الرسالة',
    'facebook' => 'فيسبوك',
    'instagram' => 'انستجرام',
    'youtube' => 'يوتيوب',
    'website' => 'الموقع',
    //announcer content
    'add_announcer' =>'اضافة مذيع',
    'image_announcer' =>'صورة المذيع',
    'nickname' =>'أسم الشهرة',
    'nickname_lan' =>'أسم الشهرة بالانجليزى ',
    'brief_lan' =>'نبذة بالانجليزى',
    'graduate_lan' =>' خريج بالانجليزى',
    'day_birth_lan' =>'مكان الميلاد بالانجليزى',
    'tower_lan' =>'البرج بالانجليزى',
    'date_birth' =>'تاريخ الميلاد',
    'brief' =>'نبذة',
    'day_birth' =>'مكان الميلاد',
    'tower' =>'البرج',
    'graduate' =>'خريج',
    //programs content
    'add_program'=>'اضافة برنامج',
    'name_program'=>'اسم البرنامج',
    'name_program_lan'=>'اسم البرنامج بالانجليزى',
    'image_blueprint'=>'صورة المخطط',
    'description'=>'الوصف التفصيلى',
    'description_lan'=>'الوصف التفصيلى بالانجليزى',
    'fulltime_program'=>'الوقت الكامل للبرنامج',
    'fulltime_program_lan'=>'الوقت الكامل للبرنامج بالانجليزى',
    'time_program'=>'وقت البرنامج',
    'day'=>'اليوم',
    'day_lan'=>'اليوم بالانجليزى',
    'Cairo'=>'القاهرة',
    'Alexandria'=>'الإسكندرية',
    'Ismailia'=>'الإسماعيلية',
    'Aswan'=>'أسوان',
    'Assiut'=>'أسيوط',
    'Luxor'=>'الأقصر',
    'Red Sea'=>'البحر الأحمر',
    'Behira'=>'البحيرة',
    'Beni Suef'=>'بني سويف',
    'Port Said'=>'بورسعيد',
    'South Sinai'=>'جنوب سيناء',
    'Giza'=>'الجيزة',
    'Dakahlia'=>'الدقهلية',
    'Damietta'=>'دمياط',
    'Sohag'=>'سوهاج',
    'Suez'=>'السويس',
    'Sharkiah'=>'الشرقية',
    'North Sinai'=>'شمال سيناء',
    'Gharbya'=>'الغربية',
    'Fayoum'=>'الفيوم',
    'Qalioubia'=>'القليوبية',
    'Qena'=>'قنا',
    'Kafr El Sheikh'=>'كفر الشيخ',
    'Matrouh'=>'مطروح',
    'Monofia'=>'المنوفية',
    'Minya'=>'المنيا',
    'New Valley'=>'الوادي الجديد',
    //TOWER
    'Capricorn'=>'الجدي',
    'Aries'=>'الدلو',
    'Aquarius'=>'الحوت',
    'Taurus'=>'الثور',
    'Gemini'=>'الجوزاء',
    'Cancer'=>'السرطان',
    'Leo'=>'الأسد',
    'Virgo'=>'العذراء',
    'Libra'=>'الميزان',
    'Scorpio'=>'العقرب',
    'Sagittarius'=>'القوس',

];