<?php

return [
    'dashboard' => 'Dashboard',
    'logout' => 'Log out',
    'login' => 'Login',
    'remember' => 'remember me',
    'add_user' => 'Add user',
    'name' => 'Name',
    'name_lan' => 'Name en',
    'email' => 'Email',
    'password' => 'Password',
    'password_confirmed' => 'Confirm Password',
    'register' => 'Register',
    'announcers' => 'Announcers',
    'programs' => 'Programs',
    'settings' => 'Settings',
    'show_all' => 'Show all',
    'add' => 'Add',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'online' => 'Online',
    'file' => 'no file chosen',
    'radio' => 'Radio 9090',
    'time_am' => 'am',
    'time_pm' => 'pm',
    //setting content
    'phone' => 'Phone Number',
    'message' => 'Message',
    'facebook' => 'Facebook',
    'instagram' => 'Instagram',
    'youtube' => 'YouTube',
    'website' => 'Website',
    //announcer content
    'add_announcer' =>'Add Announcer',
    'image_announcer' =>'Picture',
    'nickname' =>'Nickname ',
    'date_birth' =>'Birthdate',
    'brief' =>'Brief ',
    'day_birth' =>'Birth Place ',
    'tower' =>'Tower ',
    'graduate' =>'Graduate ',
    'nickname_lan' =>'Nickname en ',
    'brief_lan' =>'Brief en',
    'graduate_lan' =>'Graduate en',
    'day_birth_lan' =>'Birth Place en',
    'tower_lan' =>'Tower en',
    //programs content
    'add_program'=>'Add Program',
    'name_program'=>'Program Name',
    'name_program_lan'=>'Program Name en',
    'image_blueprint'=>'Blueprint Image',
    'description'=>'Description',
    'description_lan'=>'Description en',
    'fulltime_program'=>'Program Duration',
    'fulltime_program_lan'=>'Program Duration en',
    'time_program'=>'Program Time',
    'day'=>'Day',
    'day_lan'=>'Day en',
    //COUNTRY
    'Cairo'=>'Cairo',
    'Alexandria'=>'Alexandria',
    'Ismailia'=>'Ismailia',
    'Aswan'=>'Aswan',
    'Assiut'=>'Assiut',
    'Luxor'=>'Luxor',
    'Red Sea'=>'Red Sea',
    'Behira'=>'Behira',
    'Beni Suef'=>'Beni Suef',
    'Port Said'=>'Port Said',
    'South Sinai'=>'South Sinai',
    'Giza'=>'Giza',
    'Dakahlia'=>'Dakahlia',
    'Damietta'=>'Damietta',
    'Sohag'=>'Sohag',
    'Suez'=>'Suez',
    'Sharkiah'=>'Sharkiah',
    'North Sinai'=>'North Sinai',
    'Gharbya'=>'Gharbya',
    'Fayoum'=>'Fayoum',
    'Qalioubia'=>'Qalioubia',
    'Qena'=>'Qena',
    'Kafr El Sheikh'=>'Kafr El Sheikh',
    'Matrouh'=>'Matrouh',
    'Monofia'=>'Monofia',
    'Minya'=>'Minya',
    'New Valley'=>'New Valley',
    //TOWER
    'Capricorn'=>'Capricorn',
    'Aries'=>'Aries',
    'Aquarius'=>'Aquarius',
    'Taurus'=>'Taurus',
    'Gemini'=>'Gemini',
    'Cancer'=>'Cancer',
    'Leo'=>'Leo',
    'Virgo'=>'Virgo',
    'Libra'=>'Libra',
    'Scorpio'=>'Scorpio',
    'Sagittarius'=>'Sagittarius',


];