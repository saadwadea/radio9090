<?php


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ],
    function() {
        Auth::routes();
    });



Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['auth','localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ],
    function()
    {



        Route::get('/', array('as' => 'index', 'uses' => 'AdminController@index'));

        Route::get('users/{user}', ['as' => 'users.edit', 'uses' => 'UserController@edit']);

        Route::patch('users/{user}/update', ['as' => 'users.update', 'uses' => 'UserController@update']);

        Route::resource('announcer', 'AnnouncerController');

        Route::resource('program', 'programController');

        Route::get('setting', 'settingController@edit');

        Route::put('update_setting', 'settingController@update')->name('setting');

    });

