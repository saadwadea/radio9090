<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(
    [
//        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['localize']
    ],
    function()
    {
        Route::get('/announcer', 'api\AnnouncerController@index');
        Route::get('/program', 'api\ProgramController@index');
        Route::post('/programByDay', 'api\ProgramController@programByDay');
        Route::get('/announcerOfProgram', 'api\AnnouncerController@announcerOfProgram');
        Route::get('/programOfAnnouncer', 'api\ProgramController@programOfAnnouncer');
        Route::get('/setting', 'api\SettingController@index');
        Route::middleware('auth:api')->get('/user', function (Request $request) {
            return $request->user();
        });
    });